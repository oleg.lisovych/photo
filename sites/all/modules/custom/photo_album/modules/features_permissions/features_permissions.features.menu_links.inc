<?php
/**
 * @file
 * features_permissions.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function features_permissions_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_photo-album:admin/config/photo_album.
  $menu_links['management_photo-album:admin/config/photo_album'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/photo_album',
    'router_path' => 'admin/config/photo_album',
    'link_title' => 'Photo album',
    'options' => array(
      'attributes' => array(
        'title' => 'Administration tools.',
      ),
      'identifier' => 'management_photo-album:admin/config/photo_album',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_configuration:admin/config',
  );
  // Exported menu link: management_views-plugins:admin/reports/views-plugins.
  $menu_links['management_views-plugins:admin/reports/views-plugins'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/reports/views-plugins',
    'router_path' => 'admin/reports/views-plugins',
    'link_title' => 'Views plugins',
    'options' => array(
      'attributes' => array(
        'title' => 'Overview of plugins used in all views.',
      ),
      'identifier' => 'management_views-plugins:admin/reports/views-plugins',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_reports:admin/reports',
  );
  // Exported menu link: navigation_add-photo:photo-album/%.
  $menu_links['navigation_add-photo:photo-album/%'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'photo-album/%',
    'router_path' => 'photo-album/%',
    'link_title' => 'Add photo',
    'options' => array(
      'identifier' => 'navigation_add-photo:photo-album/%',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: navigation_photos:photo_album/photos.
  $menu_links['navigation_photos:photo_album/photos'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'photo_album/photos',
    'router_path' => 'photo_album/photos',
    'link_title' => 'Photos',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure general photo album settings, fields, and displays.',
      ),
      'identifier' => 'navigation_photos:photo_album/photos',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add photo');
  t('Photo album');
  t('Photos');
  t('Views plugins');

  return $menu_links;
}
