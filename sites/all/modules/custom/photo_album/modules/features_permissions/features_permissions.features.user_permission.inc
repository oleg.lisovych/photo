<?php
/**
 * @file
 * features_permissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function features_permissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer photos'.
  $permissions['administer photos'] = array(
    'name' => 'administer photos',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'configure photos settings'.
  $permissions['configure photos settings'] = array(
    'name' => 'configure photos settings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'create photos'.
  $permissions['create photos'] = array(
    'name' => 'create photos',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'delete any photos'.
  $permissions['delete any photos'] = array(
    'name' => 'delete any photos',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'delete own photos'.
  $permissions['delete own photos'] = array(
    'name' => 'delete own photos',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'edit any photos'.
  $permissions['edit any photos'] = array(
    'name' => 'edit any photos',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'edit own photos'.
  $permissions['edit own photos'] = array(
    'name' => 'edit own photos',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'photo_album',
  );

  // Exported permission: 'view photos'.
  $permissions['view photos'] = array(
    'name' => 'view photos',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'photo_album',
  );

  return $permissions;
}
