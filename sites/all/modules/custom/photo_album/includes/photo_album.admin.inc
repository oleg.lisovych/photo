<?php

/**
 * Page callback: Builds the photo album administration overview.
 */
function photo_album_admin_photos() {
	$header = array(
		'title' => array('data' => t('Title'), 'field' => 'p.title'),
		'author' => t('Author'),
		'changed' => array('data' => t('Updated'), 'field' => 'p.changed', 'sort' => 'desc'),
		'operations' => array('data' => t('Operations')),
	);
	
	$query = db_select('photo_album', 'p')->extend('PagerDefault')->extend('TableSort');
	$photo_album_ids = $query->fields('p', array('id'))
		->limit(50)
		->orderByHeader($header)
		->execute()
		->fetchCol();
	
	$photos = photo_album_load_multiple($photo_album_ids);
	
	$destination = drupal_get_destination();
	$rows = array();
	foreach ($photos as $photo_album) {
		$rows[$photo_album->id] = array(
			'title' => array(
				'data' => array(
					'#type' => 'link',
					'#title' => $photo_album->title,
					'#href' => 'photo_album/' . $photo_album->id,
				),
			),
			'author' => theme('username', array('account' => $photo_album)),
			'changed' => format_date($photo_album->changed, 'short'),
		);
		
		$operations = array();
		if (photo_album_access('update', 'photo_album', $photo_album)) {
			$operations['edit'] = array(
				'title' => t('edit'),
				'href' => 'photo_album/' . $photo_album->id . '/edit',
				'query' => $destination,
			);
		}
		if (photo_album_access('delete', 'photo_album', $photo_album)) {
			$operations['delete'] = array(
				'title' => t('delete'),
				'href' => 'photo_album/' . $photo_album->id . '/delete',
				'query' => $destination,
			);
		}
		
		$rows[$photo_album->id]['operations'] = array();
		if (count($operations) > 1) {
			$rows[$photo_album->id]['operations'] = array(
				'data' => array(
					'#theme' => 'links__photo_album_operations',
					'#links' => $operations,
					'#attributes' => array('class' => array('links', 'inline')),
				),
			);
		}
		elseif (!empty($operations)) {
			$link = reset($operations);
			$rows[$photo_album->id]['operations'] = array(
				'data' => array(
					'#type' => 'link',
					'#title' => $link['title'],
					'#href' => $link['href'],
					'#options' => array('query' => $link['query']),
				),
			);
		}
	}
	
	$page = array();
	$page['photos'] = array(
		'#theme' => 'table',
		'#header' => $header,
		'#rows' => $rows,
		'#empty' => t('No photos available.'),
	);
	
	$page['pager'] = array('#markup' => theme('pager'));
	return $page;
}