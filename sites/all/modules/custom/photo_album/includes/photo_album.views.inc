<?php
/**
 * Implements hook_views_api().
 */
function photo_album_views_api() {
	return array(
		'api' => 3,
	);
}

/**
 * Implements hook_views_data().
 */
function photo_album_views_data() {
	$data = [];
	
	$data['photo_album']['table']['group'] = t('Photo album');
	
	$data['photo_album']['table']['base'] = [
		'field' => 'id',
		'title' => t('Photo album'),
	];
	
	$data['photo_album']['id'] = [
		'title' => t('ID'),
		'help' => t('Photo album ID.'),
		'field' => [
			'handler' => 'views_handler_field_numeric',
			'click sortable' => TRUE,
		],
		'filter' => [
			'handler' => 'views_handler_filter_numeric',
		],
		'sort' => [
			'handler' => 'views_handler_sort',
		],
	];
	
	$data['photo_album']['uid'] = array(
		'title' => t('Author'),
		'help' => t('Author of a location.'),
		'relationship' => array(
			'title' => t('Author'),
			'handler' => 'views_handler_relationship',
			'label' => t('author'),
			'base' => 'users',
			'base field' => 'uid',
			'relationship field' => 'uid',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_user_name',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_numeric',
		),
	);
	
	$data['photo_album']['title'] = [
		'title' => t('Title'),
		'help' => t('Photo album title field.'),
		'field' => [
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		],
		'sort' => [
			'handler' => 'views_handler_sort',
		],
		'filter' => [
			'handler' => 'views_handler_filter_string',
		],
		'argument' => [
			'handler' => 'views_handler_argument_string',
		],
	];
	
	$data['photo_album']['fid'] = array(
		'title' => t('File ID'),
		'help' => t('The ID of the file.'),
		'field' => array(
			'handler' => 'photo_album_views_handler_field_photo',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_boolean_operator',
			'label' => t('Has Avatar'),
			'type' => 'yes-no',
		),
	);
	
	return $data;
}
