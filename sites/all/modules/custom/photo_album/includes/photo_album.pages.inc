<?php

/**
 * Title callback: Returns the title of the photo album.
 */
function photo_album_page_title($photo_album) {
	return $photo_album->title;
}

/**
 * Menu callback: Displays a single photo album.
 */
function photo_album_page_view($photo_album) {
	$photos = photo_album_view_multiple(array($photo_album->id => $photo_album), 'full');
	
	return $photos;
}

/**
 * Constructs a drupal_render() style array from an array of loaded photos.
 */
function photo_album_view_multiple($photos, $view_mode = 'full', $langcode = NULL) {
	field_attach_prepare_view('photo_album', $photos, $view_mode, $langcode);
	entity_prepare_view('photo_album', $photos, $langcode);
	$build = array();
	$weight = 0;
	foreach ($photos as $photo_album) {
		$build['photos'][$photo_album->id] = photo_album_view($photo_album, $view_mode, $langcode);
		$build['photos'][$photo_album->id]['#weight'] = $weight++;
	}
	$build['photos']['#sorted'] = TRUE;
	return $build;
}

/**
 * Generates an array for rendering the given photo album.
 */
function photo_album_view($photo_album, $view_mode = 'full', $langcode = NULL) {
	if (!isset($langcode)) {
		global $language;
		$langcode = $language->language;
	}
	
	photo_album_build_content($photo_album, $view_mode, $langcode);
	
	$build = $photo_album->content;
	unset($photo_album->content);
	
	$build += array(
		'#theme' => 'photo_album',
		'#photo_album' => $photo_album,
		'#view_mode' => $view_mode,
		'#language' => $langcode,
	);
	
	return $build;
}

/**
 * Builds a structured array representing the photo album content.
 */
function photo_album_build_content($photo_album, $view_mode = 'full', $langcode = NULL) {
	if (!isset($langcode)) {
		global $language;
		$langcode = $language->language;
	}
	
	$photo_album->content = array();
	
	field_attach_prepare_view('photo_album', array($photo_album->id => $photo_album), $view_mode, $langcode);
	entity_prepare_view('photo_album', array($photo_album->id => $photo_album), $langcode);
	$photo_album->content += field_attach_view('photo_album', $photo_album, $view_mode, $langcode);
	
	$photo_album->content += array('#view_mode' => $view_mode);
	
	$photo_album->content['links'] = array(
		'#theme' => 'links__photo_album',
		'#pre_render' => array('drupal_pre_render_links'),
		'#attributes' => array('class' => array('links')),
	);
}

/*
 * Create a basic entity structure to be used and passed to the validation
 * and submission functions.
 */
function photo_album_page_add($ajax) {
	global $user;
	$name = variable_get('anonymous', t('Anonymous'));
	if ($user->uid && $account = user_load($user->uid)) {
		$name = $account->name;
	}
	
	$photo_album = entity_create('photo_album', array('uid' => $user->uid, 'name' => $name));
	if ($ajax) {
		ctools_include('ajax');
		ctools_include('modal');
		
		$form_state = array(
			'ajax' => TRUE,
			'title' => t('Add photo'),
			'photo_album' => $photo_album,
		);
		
		$output = ctools_modal_form_wrapper('photo_album_form', $form_state);
		
		if (!empty($form_state['ajax_commands'])) {
			$output = $form_state['ajax_commands'];
		}
		
		// Return the ajax instructions to the browser via ajax_render().
		print ajax_render($output);
		drupal_exit();
	}
	else {
		return drupal_get_form('photo_album_form', $photo_album, 'create');
	}
}

/*
 * Presents the photo album editing form.
 */
function photo_album_page_edit($photo_album) {
	drupal_set_title(t('<em>Edit</em> @title', array('@title' => $photo_album->title)), PASS_THROUGH);
	return drupal_get_form('photo_album_form', $photo_album);
}

/**
 * Form constructor for the photo album add/edit form.
 *
 * @see photo_album_validate()
 * @see photo_album_form_submit()
 * @see photo_album_form_delete_submit()
 * @ingroup forms
 */
function photo_album_form($form, &$form_state, $photo_album = NULL, $op = 'update') {
	$form['photo_album'] = array(
		'#type' => 'value',
		'#value' => $photo_album,
	);
	
	$form['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => !empty($photo_album->title) ? $photo_album->title : '',
		'#required' => TRUE,
	);
	
	$form['picture_upload'] = array(
		'#type' => 'managed_file',
		'#title' => t('Upload image'),
		'#upload_location' => "public://",
		'#default_value' => !empty($photo_album->fid) ? $photo_album->fid : NULL,
		'#required' => TRUE,
	);
	
	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Authored by'),
		'#maxlength' => 60,
		'#autocomplete_path' => 'user/autocomplete',
		'#default_value' => !empty($photo_album->name) ? $photo_album->name : '',
		'#weight' => 6,
		'#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
	);
	$form['date'] = array(
		'#type' => 'textfield',
		'#title' => t('Authored on'),
		'#maxlength' => 25,
		'#description' => t('Format: %time. The date format is YYYY-MM-DD and %timezone is the time zone offset from UTC. Leave blank to use the time of form submission.', array('%time' => !empty($photo_album->created) ? format_date($photo_album->created, 'custom', 'Y-m-d H:i:s O') : format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s O'), '%timezone' => !empty($product->created) ? format_date($product->created, 'custom', 'O') : format_date(REQUEST_TIME, 'custom', 'O'))),
		'#default_value' => !empty($photo_album->created) ? format_date($photo_album->created, 'custom', 'Y-m-d H:i:s O') : '',
	);
	
	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 5,
	);
	
	if ($op == 'update' && photo_album_access('delete', 'photo_album')) {
		$form['actions']['delete'] = array(
			'#type' => 'submit',
			'#value' => t('Delete'),
			'#weight' => 15,
			'#submit' => array('photo_album_form_delete_submit'),
		);
	}
	
	field_attach_form('photo_album', $photo_album, $form, $form_state);
	
	return $form;
}

/**
 * Form validation handler for photo_album_form().
 *
 * @see photo_album_form()
 * @see photo_album_form_submit()
 */
function photo_album_form_validate($form, &$form_state) {
	$photo_album = $form_state['values']['photo_album'];

	if (!empty($form_state['values']['name'])) {
		if ($account = user_load_by_name($form_state['values']['name'])) {
			$photo_album->uid = $account->uid;
		}
		elseif ($form_state['values']['name'] != variable_get('anonymous', t('Anonymous'))) {
			form_set_error('name', t('The username %name does not exist.', array('%name' => $form_state['values']['name'])));
		}
	}

	if (!empty($form_state['values']['date'])) {
		if (strtotime($form_state['values']['date']) === FALSE) {
			form_set_error('date', t('You have to specify a valid date.'));
		}
		else {
			$date = date_create($form_state['values']['date']);
			$photo_album->created = $date->getTimestamp();
		}
	}
	elseif (empty($photo_album->created)) {
		$photo_album->created = REQUEST_TIME;
	}
}

/**
 * Form submission handler for photo_albumform().
 *
 * @see photo_album_form()
 * @see photo_album_form_validate()
 */
function photo_album_form_submit($form, &$form_state) {
	$photo_album = !empty($form_state['values']['photo_album']) ? $form_state['values']['photo_album'] : $form_state['photo_album'];
	$photo_album->title = $form_state['values']['title'];
	$photo_album->changed = REQUEST_TIME;
	$fid = !empty($form_state['values']['picture_upload']) ? $form_state['values']['picture_upload'] : '';
	
	if(!empty($fid)) {
		$file = file_load($fid);
		$file->status = FILE_STATUS_PERMANENT;
		file_save($file);
	}
	$photo_album->fid = $fid;
	
	field_attach_submit('photo_album', $photo_album, $form, $form_state);
	
	$is_new = !empty($photo_album->is_new) ? TRUE : FALSE;
	$photo_album->save();
	
	if ($is_new) {
		drupal_set_message(t('%title has been created.', array('%title' => $photo_album->title)));
	}
	else {
		drupal_set_message(t('%title has been updated.', array('%title' => $photo_album->title)));
	}
	if ($file) {
		file_usage_add($file, 'photo_album', 'photo_album', $photo_album->id);
	}
	if(!empty($form_state['ajax'])) {
		$commands[] = ctools_modal_command_dismiss();
		print ajax_render($commands);
		exit;
	}
	else {
		$form_state['redirect'] = array('photo_album/' . $photo_album->id);
		
	}
}

/**
 * Form submission handler for photo_album_form().
 *
 * Handles the 'Delete' button on the photo album form.
 *
 * @see photo_album_form()
 * @see photo_album_form_validate()
 */
function photo_album_form_delete_submit($form, &$form_state) {
	$destination = array();
	if (isset($_GET['destination'])) {
		$destination = drupal_get_destination();
		unset($_GET['destination']);
	}
	$photo_album = $form_state['values']['photo_album'];
	$form_state['redirect'] = array('photo_album/' . $photo_album->id . '/delete', array('query' => $destination));
}
/**
 * Form constructor for the photo album deletion confirmation form.
 *
 * @see photo_album_delete_confirm_submit()
 */
function photo_album_delete_confirm($form, &$form_state, $photo_album) {
	$form['photo_album_id'] = array('#type' => 'value', '#value' => $photo_album->id);
	return confirm_form($form,
		t('Are you sure you want to delete %title?', array('%title' => $photo_album->title)),
		'photos',
		t('This action cannot be undone.'),
		t('Delete'),
		t('Cancel')
	);
}

/**
 * Executes photo album deletion.
 *
 * @see photo_album_delete_confirm()
 */
function photo_album_delete_confirm_submit($form, &$form_state) {
	if ($form_state['values']['confirm']) {
		$photo_album = photo_album_load($form_state['values']['photo_album_id']);
		photo_album_delete($form_state['values']['photo_album_id']);
		drupal_set_message(t('%title has been deleted.', array('%title' => $photo_album->title)));
	}
	
	$form_state['redirect'] = 'admin/config/photo_album/photos';
}