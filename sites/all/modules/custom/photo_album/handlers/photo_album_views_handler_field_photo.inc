<?php

/**
 * @file
 * Definition of photo_album_views_handler_field_photo.
 */

/**
 * Field handler to provide simple renderer that allows using a themed photo link.
 *
 * @ingroup views_field_handlers
 */
class photo_album_views_handler_field_photo extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['fid'] = 'fid';
    $this->additional_fields['id'] = 'id';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_photo'] = array('default' => TRUE, 'bool' => TRUE);
    $options['image_style'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_photo'] = array(
      '#title' => t("Link to photo from album"),
      '#description' => t("Link to photo from album"),
      '#type' => 'checkbox',
      '#default_value' => $this->options['link_to_photo'],
    );

    if (module_exists('image')) {
      $styles = image_styles();
      $style_options = array('' => t('Default'));
      
      foreach ($styles as $style) {
        $style_options[$style['name']] = $style['name'];
      }

      $form['image_style'] = array(
        '#title' => t('Image style'),
        '#type' => 'select',
        '#options' => $style_options,
        '#default_value' => $this->options['image_style'],
      );
    }
  }

  function render($values) {
	  $output = '';
	  
    if (
    	!$this->options['image_style'] && !module_exists('image') ||
	    empty($picture_fid = $this->get_value($values, 'fid'))
    ) {
    	return $output;
    }
    
    try {
	    if ($picture = file_load($picture_fid)) {
		    $picture_filepath = $picture->uri;
		
		    if (file_valid_uri($picture_filepath)) {
			    $output = theme('image_style', [
				    'style_name' => $this->options['image_style'],
				    'path' => $picture_filepath
			    ]);
			
			    if ($this->options['link_to_photo'] && user_access('edit own photos')) {
				    $id = $this->get_value($values, 'id');
				    $output = l($output, "photo_album/$id", ['html' => TRUE]);
			    }
		    }
	    }
    }
    catch (Exception $e) {
    	watchdog('photo_album', $e->getMessage());
    }
	
	  return $output;
  }
}
